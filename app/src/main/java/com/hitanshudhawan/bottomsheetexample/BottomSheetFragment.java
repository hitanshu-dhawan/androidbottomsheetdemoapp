package com.hitanshudhawan.bottomsheetexample;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * Created by hitanshu on 23/3/18.
 */

public class BottomSheetFragment extends BottomSheetDialogFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View bottomSheetView = inflater.inflate(R.layout.fragment_bottom_sheet_dialog, container, false);

        bottomSheetView.findViewById(R.id.preview_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "preview layout sheet dialog fragment", Toast.LENGTH_SHORT).show();
            }
        });
        bottomSheetView.findViewById(R.id.share_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "share layout sheet dialog fragment", Toast.LENGTH_SHORT).show();
            }
        });
        bottomSheetView.findViewById(R.id.link_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "link layout sheet dialog fragment", Toast.LENGTH_SHORT).show();
            }
        });
        bottomSheetView.findViewById(R.id.copy_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "copy layout sheet dialog fragment", Toast.LENGTH_SHORT).show();
            }
        });
        bottomSheetView.findViewById(R.id.email_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "email layout sheet dialog fragment", Toast.LENGTH_SHORT).show();
            }
        });

        return bottomSheetView;
    }
}
