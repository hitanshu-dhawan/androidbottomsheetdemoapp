package com.hitanshudhawan.bottomsheetexample;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button button1;
    Button button2;
    Button button3;

    LinearLayout bottomSheetLayout;
    BottomSheetBehavior sheetBehavior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        bottomSheetLayout = findViewById(R.id.bottom_sheet);
        sheetBehavior = BottomSheetBehavior.from(bottomSheetLayout);


        /////////////////////////////////
        //// Persistent Bottom Sheet ////
        /////////////////////////////////

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    //button1.setText("Close sheet");
                } else {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    //button1.setText("Expand sheet");
                }
            }
        });

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        button1.setText("Expand sheet");
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        button1.setText("Close sheet");
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        Toast.makeText(MainActivity.this, "Hidden", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });


        ////////////////////////////
        //// Modal Bottom Sheet ////
        ////////////////////////////

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View bottomSheetView = getLayoutInflater().inflate(R.layout.fragment_bottom_sheet_dialog, null);

                bottomSheetView.findViewById(R.id.preview_layout).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(MainActivity.this, "preview layout sheet dialog", Toast.LENGTH_SHORT).show();
                    }
                });
                bottomSheetView.findViewById(R.id.share_layout).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(MainActivity.this, "share layout sheet dialog", Toast.LENGTH_SHORT).show();
                    }
                });
                bottomSheetView.findViewById(R.id.link_layout).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(MainActivity.this, "link layout sheet dialog", Toast.LENGTH_SHORT).show();
                    }
                });
                bottomSheetView.findViewById(R.id.copy_layout).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(MainActivity.this, "copy layout sheet dialog", Toast.LENGTH_SHORT).show();
                    }
                });
                bottomSheetView.findViewById(R.id.email_layout).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(MainActivity.this, "email layout sheet dialog", Toast.LENGTH_SHORT).show();
                    }
                });

                BottomSheetDialog dialog = new BottomSheetDialog(MainActivity.this);
                dialog.setContentView(bottomSheetView);
                dialog.show();
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomSheetFragment bottomSheetFragment = new BottomSheetFragment();
                bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
            }
        });

    }

}
